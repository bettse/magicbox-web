# Magic box web control

Control Magicbox from the browser

Requires a browser with WebBLE support (basically Google Chrome)

Borrows heavily from ["Using Web Bluetooth with Espruino"](https://www.espruino.com/Web+Bluetooth)
